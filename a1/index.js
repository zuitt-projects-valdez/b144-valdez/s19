let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Ave", "NW", "California"];
const [street, city, state] = address;

console.log(`I live at ${street} ${city}, ${state}`);

const animal = {
  animalName: "Lucy",
  species: "frog",
  weight: "22.7 g",
  length: "7 cm",
};

const { animalName, species, weight, length } = animal;
console.log(
  `${animalName} is a ${species}. She is weighed at ${weight} with a measurement of ${length}.`
);

let nums = [5, 10, 15, 20, 25];
nums.forEach((number) => console.log(number));

let reduceNumber = nums.reduce((num1, num2) => num1 + num2);
console.log(reduceNumber);

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let dog1 = new Dog("Noodle", "3", "dachschund");
console.log(dog1);
