//JS ES6 Updates

/* 
  1. Exponent operator
*/
//pre version
const firstNum = Math.pow(8, 2);
console.log(firstNum);

//ES6
const secondNum = 8 ** 2;
console.log(secondNum);

/* 
  2. Template Literals 
  - writes strings without concatenation (+)
  - creates more readable strings in code
  - indicated with back ticks (``)
  - accommodates multi line entries
  - have embedded JS expressions (${})
    - any valid unit of code that resolves to a value 
*/

//pre template literal string
let name = "John";
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: " + message);

//strings using template literals (``)
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

//multi line template literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${secondNum}`;
console.log(anotherMessage);

//computing within the template literal
const interestRate = 0.1;
const principal = 1000;
console.log(
  `The interest on your savings account is: ${principal * interestRate}`
);

/* 
  Array destructuring 
  - unpacks/destructure
  - unpacks elements in arrays into distinct variables 
  - names array elements with her variables instead of using index numbers 
  - helps with code readability 
  - syntax: let/const [variableName, variableName] = array
*/
const fullName = ["Juan", "Dela", "Cruz"];

//pre array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`
);

//Array destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`
);

/* 
  Object destructuring 
  - unpacks elements in objects into distinct variable 
  - shortens the syntax for accessing properties from objects 
  - let/const {propertyName, propertyName} = objectName
    - property names in the destructuring need to match the ones in the original object
*/
const person = {
  givenName: "Jane",
  maidenName: "Dela",
  familyName: "Cruz",
};

// pre object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`
);

//object destructuring
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(
  `Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`
);

//using destructured objects in a function
function getFullName({ givenName, maidenName, familyName }) {
  console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

/* 
  Arrow functions
  - syntax: const/let variableName = () => {statement/s}
  - can have an anonymous function - no function name 
  - compact alternative syntax to traditional functions 
  - useful for code snippets where creating functions will not be reused in any other portion of the code 
  - adheres to the DRY principle (dont repeat yourself) where's there's no longer need to create a function and think for a name for functions that will only be used in a certain snippet 
*/
//normal function = function declaration, function name, parameters (placeholders/name of an argument to be passed to the function), statements, invoke/call back function
function printFullName(fName, mName, lName) {
  console.log(fName + " " + mName + " " + lName);
}

printFullName("John", "D.", "Smith");

//arrow function
const variableName = () => {
  console.log("Hello World");
};
const printFName = (fName, mName, lName) => {
  console.log(`${fName} ${mName} ${fName}`);
};

printFName("Jane", "D.", "Smith");

//arrow functions with loops
//pre arrow
const students = ["John", "Jane", "Joe"];
students.forEach(function (student) {
  console.log(`${student} is a student`);
});
//arrow function - no need to write function
students.forEach((student) => {
  console.log(`${student} is a student`);
});

/* arrow function - implicit return statement; return keyword and {} are optional with the arrow function if there is only one thing to return, can also omit () in parameters if there is only one */
//pre arrow function
const add = (x, y) => {
  return x + y;
};

let total = add(1, 2);
console.log(total);

//arrow function
const addNum = (x, y) => x + y;
let totalNum = addNum(1, 2);
console.log(totalNum);

// let filterFriends = friends.filter((friend) => friend.length === 4);

/* 
 Arrow function - default function argument value 
 - provides a default argument value if none is provided when the function is invoked   
*/

const greet = (name = "User") => {
  return `Good morning, ${name}`;
};

console.log(greet());

/* 
  Creating a class 
  - similar to functions 
  - constructor - where to pass parameters = object 
    - special method of a class for creating/initializing an object for that class
    - 'this' keyword refers to the properties of an object created from the class. 
  properties 
  - syntax: 
      class className{
        constructor(objectPropertyA, objectPropertyB){
          this.objectPropertyA = objectPropertyA;
          this.objectPropertyB = objectPropertyB;
        }
      }
*/

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

const myCar = new Car();
console.log(myCar);

//values of properties may be assigned after creation; instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

//creating/instantiation a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);

/* 
  Ternary operator 
  - conditional operator 
  - takes 3 operants 
    - condition 
    - question mark 
    - expression to execute if condition is true 
    - colon 
    - expression to execute if condition is false 
  - shorthand if else 
  - often seen in objects
*/

if (condition == 0) {
  return true;
} else {
  false;
}
//ternary operator
condition == 0 ? true : false;

{
}
